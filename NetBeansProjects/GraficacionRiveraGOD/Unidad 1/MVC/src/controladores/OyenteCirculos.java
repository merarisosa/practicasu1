package controladores;

import java.awt.Color;
import java.awt.event.*;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import modelos.*;
import vistas.PanelCirculos;

/**
 *
 * @author Rafael Rivera-Lopez
 */
public class OyenteCirculos extends MouseAdapter {

    private final Circulos circulos;
    private final PanelCirculos panel;
    private boolean seleccionado;
    private Circulo circuloSeleccionado;
    private int deltaX;
    private int deltaY;

    public OyenteCirculos(Circulos circulos, PanelCirculos panel) {
        this.circulos = circulos;
        this.panel = panel;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
//      Circulo circulo = new Circulo(e.getX(), e.getY(), 20, Color.BLUE);
//      circulos.add(circulo);

        //variable anonima
        circulos.add(new Circulo(e.getX(), e.getY(), 20, Color.BLUE));
        panel.repaint();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        for (Circulo circulo : circulos) {
            if (circulo.contiene(e.getPoint()) == true) {
                seleccionado = true;
                circuloSeleccionado = circulo;
                deltaX = e.getX() - circuloSeleccionado.x;
                deltaY = e.getY() - circuloSeleccionado.y;
                if (SwingUtilities.isRightMouseButton(e)) {
                    circuloSeleccionado.setColor(Color.pink);
                    panel.repaint();

                    int dX = e.getXOnScreen();
                    int dY = e.getYOnScreen();
                    JOptionPane dialogoConfirmacion = new JOptionPane("¿Eliminar este círculo?", JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION);
                    JDialog dialogo = dialogoConfirmacion.createDialog(panel, "Confirmación");

                    dialogo.setLocation(dX + 2, dY + 10);
                    dialogo.setSize(350, 220);
                    dialogo.setVisible(true);
                    Integer opcion = (Integer) dialogoConfirmacion.getValue();
//                    int opcion = JOptionPane.showConfirmDialog(panel, "¿Eliminar este círculo?", "Confirmación", JOptionPane.YES_NO_OPTION);
                    if (opcion == JOptionPane.YES_OPTION) {
                        circulos.remove(circuloSeleccionado);
                        panel.repaint();
                    } else {
                        circuloSeleccionado.setColor(Color.BLUE);
                        panel.repaint();
                    }
                }
                break;
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        seleccionado = false;
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (seleccionado = true) {
            circuloSeleccionado.x = e.getX() - deltaX;
            circuloSeleccionado.y = e.getY() - deltaY;
            panel.repaint();
        }
    }
}
