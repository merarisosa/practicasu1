package modelos;

import java.awt.Graphics;

/**
 *
 * @author Rafael Rivera-López
 */
public interface Dibujable {
  
  public void dibujar(Graphics g);
  
}
