package modelos;

import java.awt.Color;
import java.awt.Graphics;

/**
 * @author Rafael Rivera-López
 */
public class Bezier extends Circulos {

  public static final int CUADRATICA = 3;
  public static final int CUBICA = 4;
  private int tipoCurva;
  private Color colorCurva;

  public Bezier() {
    this(Color.BLACK);
  }

  public Bezier(Color color) {
    this.colorCurva = color;
  }

  @Override
  public void dibujar(Graphics g) {
    super.dibujar(g);
    if (this.size() == tipoCurva) {
      double inc = 0.005;
      g.setColor(colorCurva);
      Circulo p0 = get(0);
      Circulo p1 = get(1);
      Circulo p2 = get(2);
      switch (tipoCurva) {
        case CUADRATICA: {
          for (double t = 0; t <= 1; t += inc) {
            double x = Math.pow(1 - t, 2) * p0.x + 2 * t * (1 - t) * p1.x
              + t * t * p2.x;
            double y = Math.pow(1 - t, 2) * p0.y + 2 * t * (1 - t) * p1.y
              + t * t * p2.y;
            g.fillOval((int) x - 2, (int) y - 2, 4, 4);
          }
          break;
        }
        case CUBICA: {
          Circulo p3 = get(3);
          for (double t = 0; t <= 1; t += inc) {
            double x = Math.pow(1 - t, 3) * p0.x
              + 3 * Math.pow(1 - t, 2) * t * p1.x
              + 3 * (1 - t) * t * t * p2.x
              + t * t * t * p3.x;
            double y = Math.pow(1 - t, 3) * p0.y
              + 3 * Math.pow(1 - t, 2) * t * p1.y
              + 3 * (1 - t) * t * t * p2.y
              + t * t * t * p3.y;
            g.fillOval((int) x - 2, (int) y - 2, 4, 4);
          }
        }
      }
    }
  }

  public int getTipo() {
    return tipoCurva;
  }

  public void setTipo(int tipoCurva) {
    this.tipoCurva = tipoCurva;
  }

}
