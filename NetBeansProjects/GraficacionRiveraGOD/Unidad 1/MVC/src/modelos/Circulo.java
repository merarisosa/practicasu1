package modelos;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

/**
 * @author Rafael Rivera-López
 */
public class Circulo extends Point implements Dibujable {

    private int radio;
    private Color color;

    public Circulo(int x, int y, int radio, Color color) {
        super(x, y);
        this.radio = radio;
        this.color = color;
    }

    public Circulo() {
        this(0, 0, 0, Color.BLACK);
    }

    public boolean contiene(int pX, int pY) {
        double d = this.distance(pX, pY);
        if (d <= this.radio) {
            return true;
        } else {
            return false;
        }
    }

    public boolean contiene(Point p) {
        return contiene(p.x, p.y); 
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }

    @Override
    public void dibujar(Graphics g) {
        g.setColor(color);
        g.fillOval(x - radio, y - radio, 2 * radio, 2 * radio);
        g.setColor(Color.BLACK);
        g.drawOval(x - radio, y - radio, 2 * radio, 2 * radio);
    }

    public int getRadio() {
        return radio;
    }

    public void setRadio(int radio) {
        this.radio = radio;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
