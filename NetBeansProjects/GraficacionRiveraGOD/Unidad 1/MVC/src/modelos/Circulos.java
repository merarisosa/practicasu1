package modelos;

import java.awt.Graphics;
import java.util.ArrayList;

/**
 * @author Rafael Rivera-Lopez
 */
public class Circulos extends ArrayList<Circulo> implements Dibujable{

    @Override
    public void dibujar(Graphics g) {
    for(Circulo circulos:this) {
        circulos.dibujar(g);
    }   
        
    }

}
