package vistas;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import javax.swing.JPanel;
import modelos.Circulos;

/**
 * @author Rafael Rivera-Lopez
 */
public class PanelCirculos extends JPanel {

    private final Circulos circulos;

    public PanelCirculos() {
        this(new Circulos());
    }

    //vista conoce al modelo
    public PanelCirculos(Circulos circulos) {
        this.circulos = circulos;
        this.setBackground(Color.CYAN);
    }
    
//    public void addEventos(OyenteCirculos oyente){
    //oyente circulos hereda de mouseAdapter
        public void addEventos(MouseAdapter oyente){
        this.addMouseListener(oyente);
        this.addMouseMotionListener(oyente);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        circulos.dibujar(g);
    }
}
