/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manejocirculos;

import controladores.OyenteCirculos;
import java.awt.Color;
import javax.swing.JFrame;
import modelos.Circulo;
import modelos.Circulos;
import vistas.PanelCirculos;

/**
 *
 * @author merarimaysosa
 */
public class ManejoCirculos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Circulos circulos = new Circulos();                    //MODELO
//        circulos.add(new Circulo(100, 100, 50, Color.BLUE));
//        circulos.add(new Circulo(200, 150, 30, Color.RED));
//        circulos.add(new Circulo(400, 300, 100, Color.GREEN));
        
        PanelCirculos panel = new PanelCirculos(circulos);     //VISTA
        
        OyenteCirculos oyente = new OyenteCirculos(circulos,panel);
        panel.addEventos(oyente);
        
        JFrame f = new JFrame("Manejo de Círculos");
        f.setSize(800, 600);
        f.setLocation(100, 100);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.add(panel);
        f.setVisible(true);
    }

}
